document.addEventListener("DOMContentLoaded", function () {
    var phoneInputs = document.querySelectorAll('input[data-tel-input]');

    var getInputNumbersValue = function (input) {
        // Return stripped input value — just numbers
        return input.value.replace(/\D/g, '');
    }

    var onPhonePaste = function (e) {
        var input = e.target,
            inputNumbersValue = getInputNumbersValue(input);
        var pasted = e.clipboardData || window.clipboardData;
        if (pasted) {
            var pastedText = pasted.getData('Text');
            if (/\D/g.test(pastedText)) {
                // Attempt to paste non-numeric symbol — remove all non-numeric symbols,
                // formatting will be in onPhoneInput handler
                input.value = inputNumbersValue;
                return;
            }
        }
    }

    var onPhoneInput = function (e) {
        var input = e.target,
            inputNumbersValue = getInputNumbersValue(input),
            selectionStart = input.selectionStart,
            formattedInputValue = "";

        if (!inputNumbersValue) {
            return input.value = "";
        }

        if (input.value.length != selectionStart) {
            // Editing in the middle of input, not last symbol
            if (e.data && /\D/g.test(e.data)) {
                // Attempt to input non-numeric symbol
                input.value = inputNumbersValue;
            }
            return;
        }

        if (["7", "8", "9"].indexOf(inputNumbersValue[0]) > -1) {
            if (inputNumbersValue[0] == "9") inputNumbersValue = "7" + inputNumbersValue;
            var firstSymbols = (inputNumbersValue[0] == "8") ? "8" : "+7";
            formattedInputValue = input.value = firstSymbols + " ";
            if (inputNumbersValue.length > 1) {
                formattedInputValue += '(' + inputNumbersValue.substring(1, 4);
            }
            if (inputNumbersValue.length >= 5) {
                formattedInputValue += ') ' + inputNumbersValue.substring(4, 7);
            }
            if (inputNumbersValue.length >= 8) {
                formattedInputValue += '-' + inputNumbersValue.substring(7, 9);
            }
            if (inputNumbersValue.length >= 10) {
                formattedInputValue += '-' + inputNumbersValue.substring(9, 11);
            }
        } else {
            formattedInputValue = '+' + inputNumbersValue.substring(0, 16);
        }
        input.value = formattedInputValue;
    }
    var onPhoneKeyDown = function (e) {
        // Clear input after remove last symbol
        var inputValue = e.target.value.replace(/\D/g, '');
        if (e.keyCode == 8 && inputValue.length == 1) {
            e.target.value = "";
        }
    }
    for (var phoneInput of phoneInputs) {
        phoneInput.addEventListener('keydown', onPhoneKeyDown);
        phoneInput.addEventListener('input', onPhoneInput, false);
        phoneInput.addEventListener('paste', onPhonePaste, false);
    }
})

$(document).ready(function () {
	$("body").css({'visibility': "visible", "opacity": "1"});
	// forms();

	$('.search').click(function() {
		$(this).addClass('active');
	})
	
	
	// window.addEventListener('scroll', _.throttle(lazyLoad, 16))
	// window.addEventListener('resize', _.throttle(lazyLoad, 16))
	


	
	$(".sticky-block").stick_in_parent();
	if ($(window).width() < 1025) {
	}

	$(document).ready(function() {
		$('select').niceSelect();
	});




	

	
	
	new WOW().init();

	$(".collapsible-body").each(function() {
		var height = $(this).height();
		$(this).css("height", 0);
		$(this).attr("data-height", height)
	});

	$(".collapsible-header").click(function() {
		var body = $(this).next(".collapsible-body");
		var header = $(this);
		if(header.hasClass("active")) {
			header.removeClass("active");
			var height = body.height();
			// body.attr("data-height", height);
			body.height(0);
			body.removeClass("active");
		} else {


			// $(".collapsible-header").removeClass("active");
			// var height = body.height();
			// $(".collapsible-body").attr("data-height", height);
			// $(".collapsible-body").height(0);
			// $(".collapsible-body").removeClass("active");
			
			
			
			
			header.addClass("active");
			var height = body.attr("data-height");
			body.height(height);
		}
	});

	$(".noUi-handle-lower").each(function() {
		var val = +$(this).find("span").text();
		$(this).find("span").text(val.toFixed(0))
		$(".filter-output-min").text(val.toFixed(0));
	});
	$(".noUi-handle-upper").each(function() {
		var val = +$(this).find("span").text();
		$(this).find("span").text(val.toFixed(0))
		$(".filter-output-max").text(val.toFixed(0));
	});

	$(".html > img").each(function() {
		$(this).wrap("<figure></figure>")
	});

	$('.form').each(function() {
        var it = $(this);
         it.validate({
			rules: {
				message: {
					required: true,
				},
				name: {
					required: true,
				},
				nameProd: {
					required: true,	
				},
				check: {
					required: true,
				}
			},

			errorPlacement: function (error, element) {
			},

			submitHandler: function() {
				$.ajax({
					success: function(){
						$(".thanx-trigger").trigger("click");
					}
				});
			},  
         });
	 });

	$('body').on('click','[data-popup]',function(e) { //Вызов попапов
    	e.preventDefault();
    	var popup = $(this).data('popup');
    	var width = $('.blur').prop('scrollWidth');
    	$('html').addClass('no-scroll');
    	$('body').css('width',width);
    	$('.blur').addClass('active');
		$('.popup').removeClass('active');
    	$('.popup-'+popup).addClass('active');
    });
    $('body').on('mousedown','.blur',function(e) { //Закрытие попапов по .blur
    	if (this == e.target) {
    		$('.popup').removeClass('active');
    		$('html').removeClass('no-scroll');
    		$('body').css('width','auto');
			$('.blur').removeClass('active');
			$('.popup').each(function() {
				$(this).find('input[type=text],input[type=mail],textarea').val('');
				$(this).find('input[type=checkbox]').prop('checked', false);
				$(this).find('.active').removeClass('active');
			});
			$(".header__right").removeClass("active");
			$(".catalog__sidebar-container").removeClass('active');

    	}
	});



	$(".fs-hdr").each(function() {
		var screenHeight = $(window).height();
		if ($(window).scrollTop() >= screenHeight) {
			$(this).addClass("fixed");
		}
		if ($(window).scrollTop() < screenHeight) {
			$(this).removeClass("fixed");
		}
	});
	$(window).on('scroll', function () {
		$(".fs-hdr").each(function() {
			var screenHeight = $(window).height();
			if ($(window).scrollTop() >= screenHeight) {
				$(this).addClass("fixed");
			}
			if ($(window).scrollTop() < screenHeight) {
				$(this).removeClass("fixed");
			}
		});
		if ($(window).scrollTop() > 300) {
			$(".first-mouse").addClass("active");
		} else {
			$(".first-mouse").removeClass("active");		
		}
	});
	if ($(window).scrollTop() > 300) {
		$(".first-mouse").addClass("active");
	} else {
		$(".first-mouse").removeClass("active");		
	}

    $('body').on('click','.popup__close',function(e) { //Закрытие попапов по .popup__close
		$('.popup').removeClass('active');
		$('html').removeClass('no-scroll');
		$('body').css('width','auto');
		$('.blur').removeClass('active');
		$('.popup').each(function() {
			$(this).find('input[type=text],input[type=mail],textarea').val('');
			$(this).find('input[type=checkbox]').prop('checked', false);
			$(this).find('.active').removeClass('active');
		});
	});

	if($("#fullpage").length) {
		if ($(window).width() < 1280) {
			$(".fp-tableCell, .fp-table").css("height", "auto");
			$('select').niceSelect();
			$("#fullpage").css({"position": "static", "transform": "none"});
			$.fn.fullpage.destroy();
		}
		$('#fullpage').fullpage({
			scrollingSpeed: 1000,
			autoScrolling:true,
			scrollHorizontally: true,
			navigation: false,
			slidesNavigation: false,
			slidesNavPosition: 'bottom',
			afterLoad: function(index, direction){
				var index = $(".section.active").index() + 1;
				$(".blt").removeClass("hidden");
				$(".blt").removeClass("white");
				$(".blt__item").removeClass("active")
				$(".blt__item:nth-child("+index+")").addClass("active")
				if ($(".section.active").hasClass("serv__section")) {
					$(".blt").addClass("hidden");
				}
				if ($(".section.active").hasClass("no-pag")) {
					$(".blt").addClass("hidden");
				}
				if ($(".section.active").hasClass("blue")) {
					$(".blt").addClass("white");
				}
				$(".blt__cur").text("0" + index);

			 }
		});	
	}

	$(".header__mobile").click(function() {
		$(".header__menu").addClass("active");
	});

	$(".header__close").click(function() {
		$(".header__menu").removeClass("active");
	});

	$(".header__left").click(function() {
		$(this).toggleClass("active");
		$(".header__menu").toggleClass("active");
	});


	
	$(".info__ctg-item img").click(function(e) {
		$(".info__slider").addClass("active");
		$(".info__slide").removeClass("active");
		e.preventDefault();
		var index = $(this).closest(".info__ctg-item").index() + 1;
		$(".info__nav-link").removeClass("active");
		// $(this).addClass("active");
		$(".info__slide:nth-child("+index+")").addClass("active");
		$(".info__nav-link:nth-child("+index+")").addClass("active");
	});

	$(".info__close").click(function() {
		$(".info__slider").removeClass("active");
		$(".info__slide").removeClass("active");
		$(".info__nav-link").removeClass("active");
	})

	$(".info__nav-link").click(function(e) {
		e.preventDefault();
		var index = $(this).index() + 1;
		$(".info__nav-link").removeClass("active");
		$(this).addClass("active");
		$(".info__slide").removeClass("active");
		$(".info__slide:nth-child("+index+")").addClass("active");

	});

	
	$(".m-bg-cont").each(function() {
		var img = $(this).find("img:first-of-type").attr("src");
		$(this).css("background-image", "url(" + img + ")");
	});
	 


	// NEW TTT



	SmoothScroll({
		// Время скролла 400 = 0.4 секунды
		animationTime: 500,
		// Размер шага в пикселях 
		stepSize: 100,
	
		// Дополнительные настройки:
	
		// Ускорение 
		accelerationDelta: 30,
		// Максимальное ускорение
		accelerationMax: 2,
	
		// Поддержка клавиатуры
		keyboardSupport: true,
		// Шаг скролла стрелками на клавиатуре в пикселях
		arrowScroll: 50,
	
		// Pulse (less tweakable)
		// ratio of "tail" to "acceleration"
		pulseAlgorithm: true,
		pulseScale: 4,
		pulseNormalize: 1,
	
		// Поддержка тачпада
		touchpadSupport: true,
	  })


	var mouseCursor = $('.cursort')

	window.addEventListener('mousemove',cursor)
	// window.addEventListener('scroll',cursort)

	function cursor(e) {
		// console.log(e);
		// mouseCursor.style.top = e.pageY + 'px';
		// mouseCursor.style.left = e.pageX + 'px';
		mouseCursor.css("top", e.clientY)
		mouseCursor.css("left", e.clientX)
		// console.log(e)
	}
	// function cursort(e) {
	// 	// console.log(e);
	// 	// mouseCursor.style.top = e.pageY + 'px';
	// 	// mouseCursor.style.left = e.pageX + 'px';
	// 	mouseCursor.css("top", e.pageY)
	// 	mouseCursor.css("left", e.pageX)
	// 	console.log(e)
	// }
	
	$('.footer__items-left').mouseover(function() {
		mouseCursor.addClass('active2')
		$('body').css("cursor", "none")
		console.log('222222');
	})
	$('.footer__items-left').mouseleave(function() {
		mouseCursor.removeClass('active2')
		$('body').css("cursor", "auto")
	})

	
	$('.footer__items-right').mouseover(function() {
		mouseCursor.addClass('active')
		$('body').css("cursor", "none")
	})
	$('.footer__items-right').mouseleave(function() {
		mouseCursor.removeClass('active')
		$('body').css("cursor", "auto")
	})


	$('.footer__items-right').click(function() {
		$('.cursort__next').addClass('active')
		setTimeout(function() {
			$('.cursort__next').removeClass('active')
		}, 400)
	})
	$('.footer__items-left').click(function() {
		$('.cursort__next').addClass('active2')
		setTimeout(function() {
			$('.cursort__next').removeClass('active2')
		}, 400)
	})

	var swiper = new Swiper('.footer__swiper', {
        slidesPerView: 3,
		spaceBetween: 100,
        loop: true,
        navigation: {
          nextEl: ".footer__items-right",
          prevEl: ".footer__items-left",
        },
		breakpoints: {
			// when window width is >= 320px
			600: {
				slidesPerView: 1,
			  },
			800: {
				slidesPerView: 2,
				spaceBetween: 32
			  },
			1100: {
			  slidesPerView: 2,
			  spaceBetween: 70
			},
			// when window width is >= 480px
			1630: {
			  slidesPerView: 3,
			  spaceBetween: 70
			},
			// when window width is >= 640px
			1920: {
			  slidesPerView: 3,
			  spaceBetween: 100
			}
		  }
	});

	var swiper = new Swiper('.ser__items-swiper', {
        slidesPerView: 1,
		spaceBetween: 20,
        loop: true,
	});

	// setTimeout(function() {
	// 	$('.footer__items').click(function() {
	// 		$('.cursort__next').addClass('active')
	// 	})
	// }, 1000)
	
})
